import sys,csv

#get file as input
file_name = sys.argv[1]
f = open(file_name)
voting_dictionary = csv.DictReader(f, delimiter=',')



#create lists
vote_per_candidate = {}



for row in voting_dictionary:
    voter = (row['Voter ID'])
    county = (row['County'])
    candidate = (row['Candidate'])
    if candidate in vote_per_candidate:
        vote_per_candidate[candidate] = vote_per_candidate[candidate] + 1
    else:
        vote_per_candidate[candidate] = 1

total_votes = sum(vote_per_candidate.values())

#calculating percent per candidate
votes_per_each = {}
for key,value in vote_per_candidate.items():
    x = round(value/total_votes * 100,1)
    votes_per_each[key] = x

#winner
winner = ''
max_votes = 0
for key,value in vote_per_candidate.items():
    if max_votes < value:
        winner = key
        max_votes = value
#final printout
print('\n')
print('\n')
print('Election Results')
print('--------------------------')
print('Total Votes: ',total_votes)
print('--------------------------')
#votes per candidate
for cand_name in vote_per_candidate.keys():
    print(cand_name+': ' +str(votes_per_each[cand_name])+'% ('+str(vote_per_candidate[cand_name])+')')
print('--------------------------')
print('Winner: '+winner)
print('--------------------------')

f.close()

outfile = sys.argv[2]
w = open(outfile,'w')
w.write('\n')
w.write('\n')
w.write('Election Resultsi \n')
w.write('--------------------------\n')
w.write('Total Votes: '+str(total_votes)+'\n')
w.write('--------------------------\n')
#votes per candidate
for cand_name in vote_per_candidate.keys():
    w.write(cand_name+': ' +str(votes_per_each[cand_name])+'% ('+str(vote_per_candidate[cand_name])+')\n')
w.write('--------------------------\n')
w.write('Winner: '+winner+'\n')
w.write('--------------------------\n')
